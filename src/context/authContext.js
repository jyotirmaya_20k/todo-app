import React, { useContext, useState, useEffect} from 'react'
import { useHistory } from 'react-router-dom'
import { auth, googleProvider } from '../firebase/firebase'
import { Backdrop, CircularProgress } from '@material-ui/core'
export const AuthContext = React.createContext()

export function useAuth(){
    return useContext(AuthContext)
}

export function AuthProvider({children}) {
    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)
    let history =  useHistory()
    const signup = (email, password) =>{
        return auth.createUserWithEmailAndPassword(email, password)
    }

    const signin = (email, password) =>{

        return auth.signInWithEmailAndPassword(email, password)
    }

    const signout = () =>{

        return auth.signOut()
    }

    const googleSignin = () =>{
        return auth.signInWithRedirect(googleProvider)
    }

    useEffect(() => {
       const unSubscribe = auth.onAuthStateChanged(user => {
                setCurrentUser(user)
                console.log("userset")
                setLoading(false)
                
            })

        return unSubscribe
    }, [])

    useEffect(()=>{
        auth.getRedirectResult().then(function(result) {
            console.log(result)
            if (result.credential) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                // var token = result.credential.accessToken;
                // ...
                // The signed-in user info.
                var user = result.user;
                setCurrentUser(user)
                history.push('/home')
            }
        }).catch(function(error) {
            console.log(error)
        });
    },[history])

    const value = {
        currentUser,
        signup,
        signin,
        signout,
        googleSignin
    }
    
    return (
        <AuthContext.Provider value={value}>
            {
            loading && 
            <Backdrop open={loading}>
                <CircularProgress color="inherit" />
            </Backdrop>
            }
            { !loading && children}
        </AuthContext.Provider>
    )
}
