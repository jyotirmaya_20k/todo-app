import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";

// material ui compoonents
import Paper from "@material-ui/core/Paper";
import { TextField, CircularProgress } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";

// AUthContext
import { useAuth } from "../context/authContext";

const BasicInfoForm = (props) => {
    let history = useHistory();
    // handling form validation
    const { register, handleSubmit, errors } = useForm();

    // authContext
    const { currentUser } = useAuth();
    // states
    const [errMessage, setErrMessage] = useState("");
    const [alert, setAlert] = useState("hidden");
    const [loading, setLoading] = useState(false);

    // handle form submition
     async function onSubmit (formData) {
        setAlert("hidden");
        setLoading(true);
        
        currentUser.updateProfile({
            displayName: formData.displayName
        }).then((res)=>{
            history.push('/home')
        }).catch((error)=>{
            setErrMessage(error.message);
            setAlert("");
            setLoading(false);
        })

    }


    return (
        <>
            <Paper elevation={3} className="py-3 px-8 mt-24 bg-white">
                <h2 className=" text-center">Basic Info</h2>
                <div className="">
                    <form
                        className="row py-4 space-y-4"
                        autoComplete="off"
                        noValidate
                        onSubmit={handleSubmit(onSubmit)}
                    >
                        <Alert
                            severity="error"
                            variant="filled"
                            elevation={6}
                            className={"w-full " + alert}
                        >
                            {errMessage}
                        </Alert>

                        <div className="col-12">
                            <div className="row">
                                <div className="col-12">
                                    <TextField
                                        type="text"
                                        name="displayName"
                                        inputRef={register({
                                            required: {value: true, message:"Display Name is required"}
                                        })}
                                        error={errors.displayName ? true : false}
                                        id="displayName"
                                        label="Display Name"
                                        fullWidth
                                        autoComplete="off"
                                        helperText={
                                            errors.displayName ?  errors.displayName.message : ""
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                        
                        <div className=" col-12">
                            <div className="row justify-content-center">
                                {loading ? (
                                    <CircularProgress />
                                ) : (
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                        disabled={loading}
                                    >
                                        Add
                                    </Button>
                                )}
                            </div>
                        </div>
                    </form>

                </div>
            </Paper>
        </>

    );
};

export default BasicInfoForm;
