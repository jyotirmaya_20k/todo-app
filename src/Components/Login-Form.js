import React, { useState} from "react";
import { useHistory,Link } from "react-router-dom";
import { useForm } from "react-hook-form";

// material ui compoonents
import Paper from "@material-ui/core/Paper";
import { TextField, CircularProgress,Divider } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";
import GoogleSignUp from '../Components/GoogleSignUp';

// AUthContext
import { useAuth } from "../context/authContext";

const LoginForm = (props) => {
    let history = useHistory();

    // handling form validation
    const { register, handleSubmit, errors } = useForm();

    // authContext
    const { signin } = useAuth();

    // states
    const [errMessage, setErrMessage] = useState("");
    const [alert, setAlert] = useState("hidden");
    const [loading, setLoading] = useState(false);

    // handle form submition
     async function onSubmit (formData) {
        setAlert("hidden");
        setLoading(true);
        try {
            await signin(formData.email, formData.password)
            history.push('/home')
        } catch (error) {
            setErrMessage(error.message);
            setAlert("");
            setLoading(false);
        }
    }

    return (
        <Paper elevation={3} className="py-3 px-8 mt-32 bg-white">
            <h2 className=" text-center">TO DO</h2>
            <div className="">
                <form
                    className="row py-4 space-y-4"
                    autoComplete="off"
                    noValidate
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Alert
                        severity="error"
                        variant="filled"
                        elevation={6}
                        className={"w-full " + alert}
                    >
                        {errMessage}
                    </Alert>
                    <div className="col-12">
                        <TextField
                            type="email"
                            name="email"
                            inputRef={register({
                                required: true, message:"email is required",
                                pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                            })}
                            error={errors.email ? true : false}
                            id="email"
                            label="Email"
                            fullWidth
                            autoComplete="off"
                            helperText={
                                errors.email ? "Please Enter an Valid Email" : ""
                            }
                        />
                    </div>
                    <div className="col-12">
                        <TextField
                            name="password"
                            inputRef={register({ required: true })}
                            error={errors.password ? true : false}
                            type="password" 
                            id="password"
                            label="Password"
                            fullWidth
                            helperText={
                                errors.password ? "This field is required" : ""
                            }
                        />
                    </div>
                    <div className=" col-12">
                        <div className="row justify-content-center">
                            {loading ? (
                                <CircularProgress />
                            ) : (
                                <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    disabled={loading}
                                >
                                    Login
                                </Button>
                            )}
                        </div>
                    </div>
                    <div className=" col-12">
                        
                        <div className="text-lg text-center">Or</div>
                        
                    </div>
                    <div className=" col-12">
                        <div className="row justify-content-center">
                            <GoogleSignUp />
                        </div>
                    </div>
                </form>

            </div>
            <Divider className=" -mx-10" />
            <div className="row justify-center mt-3">
                
                <div className="text-sm">
                    Don't have an Account?  
                    <Link to="/signup" className="ml-3">Signup</Link>
                </div>
            </div>
        </Paper>
    );
};

export default LoginForm;
