import React from 'react'
import InfoCard from "./InfoCard";
import {List, CheckBox, CheckBoxOutlineBlank} from '@material-ui/icons';
import MediaQuery from 'react-responsive'

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
export default function InfoCardContainer() {
    return (
        <>
            <MediaQuery minWidth={640} >   
            <section className=" flex justify-around">
                <div>
                    <InfoCard
                        title=" Created "
                        value={10}
                        icon={
                            <List className="text-green-600 text-4xl rounded my-5" />
                        }
                    />
                </div>
                <div >
                    <InfoCard title="Completed" 
                        type = "completed"
                        icon={
                            <CheckBox className="text-blue-600 text-4xl rounded my-5" />
                        } 
                    />
                </div>
                <div >
                    <InfoCard title="InComplete" type="inComplete"
                        icon={
                            <CheckBoxOutlineBlank className="text-gray-600 text-4xl rounded my-5" />
                        } 
                    />
                </div>
            </section>
            </MediaQuery>
            <MediaQuery maxWidth={639}>
                <Swiper
                    spaceBetween={20}
                    slidesPerView={1.5}
                    className="py-5"
                >
                    <SwiperSlide>
                        <InfoCard
                            title=" Created "
                            value={10}
                            icon={
                                <List className="text-green-600 text-4xl rounded my-5" />
                            }
                        />
                    </SwiperSlide>
                    <SwiperSlide>
                        <InfoCard title="Completed" 
                            type = "completed"
                            icon={
                                <CheckBox className="text-blue-600 text-4xl rounded my-5" />
                            } 
                        />
                    </SwiperSlide>
                    <SwiperSlide>
                        <InfoCard title="InComplete" type="inComplete"
                            icon={
                                <CheckBoxOutlineBlank className="text-gray-600 text-4xl rounded my-5" />
                            } 
                        />
                    </SwiperSlide>
                </Swiper>
            </MediaQuery>
        </>
    )
}
