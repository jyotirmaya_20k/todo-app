import React from 'react'
import { Button } from "@material-ui/core";
// AUthContext
import { useAuth } from "../context/authContext";

export default function GoogleSignUp() {
    const {googleSignin} = useAuth()
    async function onClick(){
        await googleSignin()    
    }

    return (
        <div>
            <Button
                variant="outlined"
                className=" rounded-full"
                color="primary"
                onClick={onClick}
            >
                <i className="fab fa-google mr-3"></i> Signin with Google
            </Button>
        </div>
    )
}
