import React from 'react'


const Layout = (props) =>{
    return (
        <section className="bg-login_bg h-screen w-screen">
            <div className="bg-black opacity-50 absolute w-full h-full left-0 top-0"></div>
            <div className="absolute w-full h-full">
                <div className="row justify-content-center m-0 ">
                    <div className="col-lg-4 col-md-8 col-10">
                        {props.children}
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Layout;