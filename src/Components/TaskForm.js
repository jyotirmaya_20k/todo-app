import React, {  useState } from "react";

import { TextField, Button, Switch, CircularProgress } from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";
import { database } from "../firebase/firebase";
import Alert from "@material-ui/lab/Alert";
import moment from 'moment'
// contexts
import { useAuth } from "../context/authContext";
const TaskForm = (props) => {
    // handling form validation
    const {  handleSubmit, errors, control, reset } = useForm();

    const { currentUser } = useAuth();

    const taskRef = database.ref("Tasks/" + currentUser.uid);
    // states
    const [successMessage, setSuccessMessage] = useState("");
    const [successAlert, setSuccessAlert] = useState("hidden");
    const [errMessage, setErrMessage] = useState("");
    const [alert, setAlert] = useState("hidden");
    const [loading, setLoading] = useState(false);
   
    const handleFormSubmit = (formData) =>{
        props.formType === "update" ? onUpdate(formData) : onSubmit(formData)
    }

    // handle form submition
    async function onSubmit(formData) {
    
        setAlert("hidden");
        setLoading(true);
        
        await taskRef.push({
            title: formData.title,
            description: formData.description,
            createdOn: moment().format('MMMM Do YYYY'),
            updatedOn: moment().format('MMMM Do YYYY'),
            completed: false
        },(error) =>{

            setTimeout(() => {
                if(error)
                {
                    setErrMessage(error.message);
                    setAlert("");
                }
                else{
                    setSuccessAlert("")
                    setSuccessMessage("Task Added Scuccessfully!")
                }

                setLoading(false);
                reset({title:"",description:""})

            }, 1000);

        })
        
    }

    async function onUpdate(formData)
    {
        setAlert("hidden");
        setLoading(true);
        
        await taskRef.child(props.taskKey).update({
            title: formData.title,
            description: formData.description,
            updatedOn: moment().format('MMMM Do YYYY'),
            completed: formData.completed
        },(error) =>{

            setTimeout(() => {
                if(error)
                {
                    setErrMessage(error.message);
                    setAlert("");
                }
                else{
                    setSuccessAlert("")
                    setSuccessMessage("Task updated Scuccessfully!")
                }

                setLoading(false);
               
            }, 1000);

        })
    }

    return (
        <form className="row space-y-3" onSubmit={handleSubmit(handleFormSubmit)}>
            <Alert
                severity="success"
                variant="filled"
                elevation={6}
                className={"w-full " + successAlert}
            >
                {successMessage}
            </Alert>

            <Alert
                severity="error"
                variant="filled"
                elevation={6}
                className={"w-full " + alert}
            >
                {errMessage}
            </Alert>
            <div className="col-12">
                <Controller
                    name="title"
                    control={control}
                    rules={{required:{value:true,message:"Add a Title to your task"}}}
                    defaultValue={props.formType === "update" ? props.title : ""}
                    as={
                        <TextField
                            error={errors.title ? true : false}
                            fullWidth
                            multiline
                            id="title"
                            label="Task Title"
                            variant="outlined"
                            helperText={
                                errors.title ? errors.title.message : ""
                            }
                        />
                    }
                />
            </div>
            <div className="col-12">
                <Controller
                    name="description"
                    control={control}
                    rules={{required:{value:true,message:"Add a Description to your task"}}}
                    defaultValue={props.formType === "update" ? props.description : ""}
                    as={
                        <TextField
                            error={errors.description ? true : false}
                            rows={6}
                            fullWidth
                            multiline
                            id="description"
                            label="Task Description"
                            variant="outlined"
                           
                            helperText={
                                errors.description ? errors.description.message : ""
                            }
                        />
                      }
                />
               
            </div>
            <div className="col-12">
                {props.formType === "update" ?
                    <>
                        <Controller
                            name="completed"
                            control={control}
                            defaultValue={ props.completed}
                            render={ controllProps =>
                                <Switch
                                    checked = {controllProps.value}
                                    name={controllProps.name}
                                    onChange={e=>controllProps.onChange(e.target.checked)}
                                    color="primary"
                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                            />}
                        />
                        <span>Completed</span>
                    </>  
                : ""}
               
            </div>
            <div className=" col-12">
                <div className="row justify-content-center">
                    {loading ? (
                        <CircularProgress />
                    ) : (
                        props.formType === "update" ? <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={loading}
                        >
                            Update
                        </Button>: <Button
                                variant="contained"
                                color="primary"
                                type="submit"
                                disabled={loading}
                            >
                                Add
                            </Button>
                    )}
                </div>
            </div>
        </form>
    );
};

export default TaskForm;
