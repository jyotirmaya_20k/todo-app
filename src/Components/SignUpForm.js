import React, { useState } from "react";
import { useHistory,Link } from "react-router-dom";
import { useForm } from "react-hook-form";

// material ui compoonents
import Paper from "@material-ui/core/Paper";
import { TextField, CircularProgress,Divider } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";

// AUthContext
import { useAuth } from "../context/authContext";

const SignUpForm = (props) => {
    let history = useHistory();
    // handling form validation
    const { register, handleSubmit, errors } = useForm();

    // authContext
    const { signup } = useAuth();

    // states
    const [errMessage, setErrMessage] = useState("");
    const [alert, setAlert] = useState("hidden");
    const [loading, setLoading] = useState(false);

    // handle form submition
     async function onSubmit (formData) {
        setAlert("hidden");
        setLoading(true);
        if(checkPassword(formData))
        {
            try {
                await signup(formData.email, formData.password)
                history.push('/basic-info')
            } catch (error) {
                setErrMessage(error.message);
                setAlert("");
                setLoading(false);
            }
        }
        else{
            setErrMessage("password Doesn't match")
            setAlert("")
            setLoading(false);
        }

    }
    const checkPassword = (formData)=>{
        return formData.password === formData.confirmPassword
    }

    return (
        <>
            <Paper elevation={3} className="py-3 px-8 mt-24 bg-white">
                <h2 className=" text-center">Signup</h2>
                <div className="">
                    <form
                        className="row py-4 space-y-4"
                        autoComplete="off"
                        noValidate
                        onSubmit={handleSubmit(onSubmit)}
                    >
                        <Alert
                            severity="error"
                            variant="filled"
                            elevation={6}
                            className={"w-full " + alert}
                        >
                            {errMessage}
                        </Alert>
                      
                        <div className="col-12">
                            <TextField
                                type="email"
                                name="email"
                                inputRef={register({
                                    required: {value: true, message:"Email is required"},
                                    pattern: {
                                        value:/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                                        message: "Enter a valid email"
                                    },
                                })}
                                error={errors.email ? true : false}
                                id="email"
                                label="Email"
                                fullWidth
                                autoComplete="off"
                                helperText={
                                    errors.email ? errors.email.message : ""
                                }
                            />
                        </div>
                        <div className="col-12">
                            <TextField
                                name="password"
                                inputRef={register({ required: true })}
                                error={errors.password ? true : false}
                                type="password" 
                                id="password"
                                label="Password"
                                fullWidth
                                helperText={
                                    errors.password ? "This field is required" : ""
                                }
                            />
                        </div>
                        <div className="col-12">
                            <TextField
                                name="confirmPassword"
                                inputRef={register({ required: true })}
                                error={errors.confirmPassword ? true : false}
                                type="password" 
                                id="confirmPassword"
                                label="Confirm Password"
                                fullWidth
                                helperText={
                                    errors.confirmPassword ? "This field is required" : ""
                                }
                            />
                        </div>
                        <div className=" col-12">
                            <div className="row justify-content-center">
                                {loading ? (
                                    <CircularProgress />
                                ) : (
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                        disabled={loading}
                                    >
                                        signup
                                    </Button>
                                )}
                            </div>
                        </div>
                    </form>

                </div>
                <Divider className=" -mx-10" />
                <div className="row justify-center mt-3">
                    
                    <div className="text-sm">
                        Alredy have an Account?  
                        <Link to="/" className="ml-3">Login</Link>
                    </div>
                </div>
            </Paper>
        </>

    );
};

export default SignUpForm;
