import React, {useState} from "react";
import {Link} from 'react-router-dom'
// import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from "@material-ui/core/styles";
import { CardHeader, Avatar, Button, IconButton, 
    Drawer, List, ListItem, ListItemIcon, ListItemText, Divider
} from "@material-ui/core";

import {Edit, Menu} from "@material-ui/icons"
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import NewTask from './NewTask';
import {useAuth} from '../context/authContext';
import { useHistory } from "react-router-dom";
import MediaQuery from 'react-responsive'

const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
}));

const Header = (props) => {
    let history = useHistory()
    const classes = useStyles();
    const { signout, currentUser } = useAuth()
    const [anchor, setAnchor] = useState(false)
    
    const logout = () =>{
        signout().then((res)=>{
            console.log(res)
            history.push('/')

        }).catch(err=>{
            console.log(err)
        })
    }
    const handleDrawer = (e) =>{
        setAnchor(false)
    }
    const openDrawer = (e) =>
    {
        setAnchor(true)
    }
    return (
        <>
        <div className="">

            <div className="row">
                <div className="col-md-6">
                    <CardHeader
                        avatar={
                            <Avatar
                                alt="user_avatar"
                                src={require("../assets/img/profile.jpg")}
                                className={classes.large}
                            />
                        }
                        action={
                            <>
                                <MediaQuery minWidth={640}>
                                    <IconButton aria-label="settings">
                                        <Link to='basic-info'>
                                            <Edit className="text-warning"  />
                                        </Link>
                                    </IconButton>
                                </MediaQuery>
                                <MediaQuery maxWidth={639}>
                                    <IconButton aria-label="settings" onClick={openDrawer}>
                                            
                                        <Menu className="text-black"  />
                                            
                                    </IconButton>
                                </MediaQuery>
                            </>
                       
                        }
                        title={currentUser.displayName}
                        subheader="September 14, 2016"
                        className="sm:w-fit"
                    />
                </div>
                <MediaQuery minWidth={640}>
                    <div className="col-md-6">
                        <div className="row justify-content-end pt-4 space-x-4">
                            
                            <NewTask />
                            <Button
                                variant="outlined"
                                className=" rounded-full"
                                color="default"
                                size="small"
                                startIcon={<PowerSettingsNewIcon />}
                                onClick={logout}
                            >
                                Logout
                            </Button>
                        </div>
                    </div>
                </MediaQuery>

                
            </div>
            <Drawer anchor="right" open={anchor} onClose={handleDrawer}>
                <div className="px-4 py-5">
                    <List>
                        <Link to='basic-info' className="text-decoration-none">
                            <ListItem button key="Edit Profile">
                                <ListItemIcon><Edit className="text-warning"  /></ListItemIcon>
                                <ListItemText primary={"Edit Profile"} className="text-decoration-none" />
                            </ListItem>
                        </Link>
                        <Divider />
                   
                        <ListItem button key="logout" onClick={logout} >
                            <ListItemIcon>
                                <PowerSettingsNewIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Logout"} />
                        </ListItem>
                    </List>
                    
                   
                </div>
            </Drawer>
        </div>

        </>
    );
};

export default Header;
