import React from 'react';
import convertToModal from './hoc/convertToModal';
import TaskForm from './TaskForm';
import { Button, Fab } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";

const ModalButton =  (props) =>{
        return (
            <>
            <Button
                variant="contained"
                className=" rounded-full"
                color="primary"
                size="small"
                startIcon={<AddIcon />}
                onClick={props.onClick}
            >
                New Task
            </Button> 

            </>
        );

}

const FlottingButton = (props) =>{
    return (
        <>
        <Fab color="primary" aria-label="add" className="fixed bottom-0 right-0 mr-5 mb-5" onClick={props.onClick}>
            <AddIcon />
        </Fab>
        </>
    );
}
const modalTitle = "Add an new Task"


 
export default convertToModal(ModalButton, modalTitle, TaskForm);

export const FlottingModalButton  = convertToModal(FlottingButton, modalTitle, TaskForm);
