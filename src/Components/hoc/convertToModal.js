import React, { Component } from "react";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from "@material-ui/core";

function convertToModal(
    ModalButton,
    modalTitle,
    ModalContent,
    ModalAction = null
    
) {
    class ConvertToModal extends Component {
        constructor(props) {
            super(props)
            this.state = {
                open: false
            }
        }
        
        
        handleClose = () =>
        {
            this.setState({
                open: false
            })
        }

                
        handleOpen = ()=>
        {
            this.setState({
                open: true
            })
        }

 
        render() {
            // console.log(this.props)
            return (
                
                <div>
                    <ModalButton onClick={this.handleOpen} {...this.props} />

                    <Dialog
                        onClose={this.handleClose}
                        aria-labelledby="customized-dialog-title"
                        open={this.state.open}
                    >
                        <DialogTitle
                            className="relative"
                            id="customized-dialog-title"
                            onClose={this.handleClose}
                        >
                            {modalTitle}
                            <i
                                className="fas fa-times cursor-pointer absolute top-0 right-0 mt-2 mr-4 text-gray-600"
                                onClick={this.handleClose}
                            ></i>
                        </DialogTitle>
                        <DialogContent dividers>
                            <ModalContent {...this.props} />
                        </DialogContent>
                        {ModalAction !== null ? (
                            <DialogActions>
                                <ModalAction />
                            </DialogActions>
                        ) : (
                            ""
                        )}
                    </Dialog>
                </div>
            );
        }
    }

    return ConvertToModal;
}

export default convertToModal;
