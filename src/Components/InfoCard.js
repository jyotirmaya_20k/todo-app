import React, {useState, useEffect} from "react";
import {Paper} from '@material-ui/core';
import { database } from '../firebase/firebase'
import { useAuth } from '../context/authContext'
const InfoCard  = (props) =>{
    const [completed, setCompleted] = useState(0)
    const [inComplete, setInComplete] = useState(0)
    const [created, setCreated] = useState(0)
    const {currentUser} = useAuth()
    
    const taskRef = database.ref("Tasks/"+currentUser.uid)

    useEffect(() => {
        taskRef.on("value",(snapshot)=>{
            setCreated(snapshot.numChildren())
        })  
        taskRef.orderByChild("completed").equalTo(true).on("value",(snapshot)=>{
            
            setCompleted(snapshot.numChildren())
        })  
        taskRef.orderByChild("completed").equalTo(false).on("value",(snapshot)=>{
            setInComplete(snapshot.numChildren())
        })
    }, [taskRef])

    const filterType = (type) =>
    {
        let value = 0;
        switch (type) {
            case "completed":
                value =  completed
                break;
            case "inComplete":
                value =  inComplete
                break;
            default:
                value =  created
                break;
        }
        return value
    }
    return (
        <Paper elevation={3} className="py-1 px-5  bg-white ">
            <div className="row">
                <div className=" col-4 border-0 border-r border-gray-500 border-solid">
                    {props.icon}
                </div>
                <div className="col-8">
                    <div className="text-center text-xl  mt-3">{props.title}</div>
                    <p className="text-center text-lg">
                        {
                            filterType(props.type)
                        }
                    </p>
                </div>
            </div>
        </Paper>
    )
}

export default InfoCard