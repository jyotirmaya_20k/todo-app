import React from "react";
import { Card, Checkbox } from "@material-ui/core";
import TaskActions from "./Actions";

class TaskCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            elivation: 1,
        };
    }

    onEnter = () => {
        this.setState({ elivation: 4 });
    };

    onleave = () => {
        this.setState({ elivation: 1 });
    };

    render() {
        return (
            <Card
                className="sm:w-2/3 w-4/5"
                elevation={this.state.elivation}
                onMouseEnter={this.onEnter}
                onMouseLeave={this.onleave}
            >
                <div className="row m-0 py-1">
                    <div className="col-2 border-0 border-r border-gray-500 border-solid p-0 m-0 ">
                        <Checkbox
                            id={this.props.taskKey}
                            className="w-full h-full"
                            checked={this.props.completed}
                            onClick={ this.props.updateCompleted}
                            color="primary"
                            inputProps={{ "aria-label": "secondary checkbox" }}
                        />
                    </div>
                    <div className="col-8">
                        <div className="space-y-2 pointer-events-none">
                            <div className="text-xl">{this.props.title}</div>

                            <div className="space-x-3 text-gray-600 space-x-2 text-xs">
                                <div className="inline-block space-x-1">
                                    <i className="fas fa-clock"></i>
                                    <span className="">
                                       
                                        {this.props.createdOn}
                                    </span>
                                </div>
                                <div className="inline-block space-x-1">
                                    <i className="fas fa-user-clock"></i>
                                    <span className="">
                                        {this.props.updatedOn}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-2">
                        <TaskActions
                            completed={this.props.completed}
                            taskKey={this.props.taskKey}
                            title={this.props.title}
                            description={this.props.description}
                            formType="update"
                        />
                    </div>
                </div>
            </Card>
        );
    }
}

export default TaskCard;
