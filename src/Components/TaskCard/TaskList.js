import React from "react";

// custom components
import TaskCard from "./index";
import { database } from "../../firebase/firebase";
import { AuthContext } from "../../context/authContext";
import moment from "moment";

import { alert, toast } from "../../helper/SweetAlert";
class TaskList extends React.Component {
    

    constructor(props) {
        super(props);
        this.state = {
            taskList: [],
            taskKeys: [],
            taskType: "completed",
        };
    }

    componentDidMount() {
        this.taskRef = database.ref("Tasks/" + this.context.currentUser.uid);

        this.taskRef.limitToFirst(20).on("child_added", (snapshot) => {
            this.updateTaskList(snapshot);
        });

        this.taskRef.limitToFirst(20).on("child_changed", (snapshot) => {
            let taskList = this.state.taskList;
            taskList.splice(
                this.state.taskKeys.indexOf(snapshot.key),
                1,
                snapshot.val()
            );
            this.setState({ taskList: taskList });
        });

        this.taskRef.limitToFirst(20).on("child_removed", (snapshot) => {
            let taskList = this.state.taskList;
            let taskKeys = this.state.taskKeys;

            taskList.splice(this.state.taskKeys.indexOf(snapshot.key), 1);
            taskKeys.splice(this.state.taskKeys.indexOf(snapshot.key), 1);

            this.setState({ taskList: taskList });
            this.setState({ taskKeys: taskKeys });
        });
    }
    updateTaskList = (snapshot) => {
        this.setState((prevState) => ({
            taskList: [snapshot.val(), ...prevState.taskList],
            taskKeys: [snapshot.key, ...prevState.taskKeys],
        }));
    };

    fetchTasks = (snapshot) => {
        let taskList = [];
        let taskKeys = [];
        snapshot.forEach((childSnapshot) => {
            taskList.push(childSnapshot.val());
            taskKeys.push(childSnapshot.key);
        });

        this.setState({
            taskList: taskList,
            taskKeys: taskKeys,
        });
    };

    markTaskAsComplete = (e) => {
        
        let taskId = e.target.id
        let completed = this.state.taskList[this.state.taskKeys.indexOf(taskId)].completed
      
        this.taskRef.child(taskId).update(
            {
                completed: !completed,
                updatedOn: moment().format("MMMM Do YYYY"),
            },
            (err) => {
                if (err) {
                    alert({
                        title: "Error!",
                        text: "Error in Updating task. Please try again!",
                        icon: "error",
                        confirmButtonText: "Ok",
                    });
                } else {
                    toast({
                        text: "Task updated successfully",
                        icon: "success",
                    });
                }
            }
        );
    };
    render() {
        return (
            <>
                {this.state.taskList.map((task, index) => (
                    <TaskCard
                        key={this.state.taskKeys[index]}
                        title={task.title}
                        createdOn={task.createdOn}
                        updatedOn={task.updatedOn}
                        completed={task.completed}
                        description={task.description}
                        taskKey={this.state.taskKeys[index]}
                        updateCompleted={this.markTaskAsComplete}
                    />
                ))}
            </>
        );
    }
}

TaskList.contextType = AuthContext;
export default TaskList;
