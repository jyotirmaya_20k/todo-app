import React from 'react'
import TaskForm from '../TaskForm'

import convertToModal from '../hoc/convertToModal'
import { IconButton} from '@material-ui/core';
import { Delete, Edit} from '@material-ui/icons';

import {useAuth} from '../../context/authContext';
import { database } from '../../firebase/firebase';

const TaskActions = (props) => {
 
    const {currentUser} = useAuth()
    const taskRef =  database.ref("Tasks/"+currentUser.uid)


    const deleteTask = (taskid) =>{
        taskRef.child(taskid).remove().then((res)=>{
            console.log("removed successfully")
        }).catch((e)=>{
            console.log(e)
        })
    }
    
    return (
        <div className="row justify-end">
             <IconButton color="default" onClick={props.onClick}   aria-label="edit" component="span">
                <Edit className="text-warning" />
            </IconButton>
            <IconButton onClick={ e => {deleteTask(props.taskKey)}} color="default"  aria-label="delete" component="span">
                <Delete className="text-danger" />
            </IconButton>
        </div>


    )
}

export default convertToModal(TaskActions,'Task Details', TaskForm)