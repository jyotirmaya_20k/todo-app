import React from 'react';
import Index from './pages/index'
import Signup from './pages/signup'
import Home from './pages/home'
import BasicInfo from './pages/basicInfo'
import { Router, Route, Switch} from "react-router-dom";
import { createBrowserHistory } from "history";
import { AuthProvider }  from './context/authContext';
import PrivateRoute from './Components/PrivateRoute'
import './assets/css/tailwind.output.css'

const hist = createBrowserHistory();

function App() {
  return (
	<Router  history={hist}>
		<AuthProvider>
			
				<Switch>
					{/* <Route path="/home" component={Home}/> */}
					<PrivateRoute path="/home" component={Home} />
					<PrivateRoute path="/basic-info" component={BasicInfo} />
					{/* <Route path="/basic-info" component={BasicInfo} /> */}
					<Route exact path="/" component={Index}/>
					<Route path="/signup" component={Signup}/>
				</Switch>
		</AuthProvider>
		
	</Router>
  );
}

export default App;
