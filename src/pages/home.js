import React from "react";

// custom components
import Header from "../Components/Header";
import TaskList from "../Components/TaskCard/TaskList";
import InfoCardContainer from '../Components/InfoCardContainer'
import { FlottingModalButton } from '../Components/NewTask'
import {AuthContext} from '../context/authContext';
import MediaQuery from 'react-responsive'


class Home extends React.Component {    
    render(){
        return (
            <div className="relative bg-blue-100 w-screen h-screen overflow-auto pb-16">
            
                <section className="container space-y-8">
                    <Header />
                    <InfoCardContainer />
                    
                    <section className="row pt-5 mt-5">
                        <div className="col-12">
                            <div className="row justify-center space-y-3 mb-20">
                                <TaskList />
                            </div>
                        </div>
                    </section>
                </section>
                <MediaQuery maxWidth={639}>
                        <FlottingModalButton />
                </MediaQuery>
            </div>

        );
    }
    
}

Home.contextType = AuthContext;
export default Home;
