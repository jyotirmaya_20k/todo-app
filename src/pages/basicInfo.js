import React from 'react'

import BasicInfoForm from '../Components/BasicInfoForm'
import Layout from '../Components/Layout'
const BasicInfo = () =>{
    return (
        <Layout>
            <BasicInfoForm />
        </Layout>
    );
}

export default BasicInfo;