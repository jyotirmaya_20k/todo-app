import React from 'react'

import Layout from '../Components/Layout'
import SignUpForm from '../Components/SignUpForm'

const Signup = () =>{
    return (
        <Layout>
            <SignUpForm />
        </Layout>
    );
}

export default Signup;