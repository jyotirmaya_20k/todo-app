import React from 'react'


import Layout from '../Components/Layout'
import LoginForm from '../Components/Login-Form'

const Index = () =>{
    return (
        <Layout>
            <LoginForm />
        </Layout>
    );
}

export default Index;