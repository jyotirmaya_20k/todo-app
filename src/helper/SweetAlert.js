import Swal from 'sweetalert2'

const alert = (props) => {
    Swal.fire({
        ...props
      })
}

const toast = (props) =>{
    Swal.fire({
        toast: true,
        position: 'top-end',
        timer: 3000,
        timerProgressBar: true,
        showConfirmButton: false,
        ...props
      })
}

export {alert, toast}