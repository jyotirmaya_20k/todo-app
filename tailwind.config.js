module.exports = {
  purge: {
    // enabled: true, // This will *always* minify, even on dev builds
    // content: ['./pages/**/*.js','./components/**/*.js',]
  },
  important: true,
  corePlugins: {
    preflight: false,
  },
  theme: {
    extend: {
      backgroundImage: theme => ({
        'login_bg': "url('../img/login_bg.jpg')"
      }),
      // colors: {
      //   'poster':'#35a0c0',
      //   blue: {
      //     100: '#BBDEFB',
      //     200: '#90CAF9',
      //     300: '#64B5F6',
      //     400: '#42A5F5',
      //     500: '#2196F3',
      //     600: '#1E88E5',
      //     700: '#1976D2'
      //   },
      //   green: {
      //     500: '#4CAF50',
      //     600: '#43A047'
      //   }
      // },
      maxHeight: {
        25: '25%',
        50: '50%',
        75: '75%',
        80: '80%',
        90: '90%',
        98: '98%',
        'time-slot': '30rem'
      },
      height: {
        fit: 'fit-content',
        25: '25%',
        50: '50%',
        75: '75%',
        80: '80%',
        90: '90%',
        '50vh' : '50vh',
        '40vh' : '40vh'
      },
      width: {
        fit: 'fit-content',
        25: '25%',
        50: '50%',
        75: '75%',
        90: '90%',
        80: "80%"
      },
      screens: {
        'max-xl': {'max': '1279px'},
        // => @media (max-width: 1279px) { ... } 
  
        'max-lg': {'max': '1023px'},
        // => @media (max-width: 1023px) { ... }
  
        'max-md': {'max': '767px'},
        // => @media (max-width: 767px) { ... }
  
        'max-sm': {'max': '639px'},
        // => @media (max-width: 639px) { ... }
      }
    },
  },
  variants: {},
  plugins: [],
}
